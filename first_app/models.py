from django.db import models
from django.db import models


class Rank(models.Model):
    rank_id = models.CharFieldId(max_digits=10)
    rank_type= models.CharFieldId(max_digits=15)

    def __str__(self):
        return f'({self.rank_id}) - ({self.rank_type})'

class Customer(models.Model):
    customer_id = models.Field(primary_key=True, default=id, editable=False)
    rank = models.ForeignKey(Rank, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    phone = models.CharField(max_length = 15)

    def __str__(self):
        return f'({self.customer_id}) - ({self.rank)} - ({self.first_name})- ({self.last_name}) - ({self.phone})' 

class Accounts(models-Model):
    account_id = models.Field(primary_key=True, default=id, editable=False) 
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    amount = models.DecimalField(max_digit=None, Decimal_places=None)
    is_loan = models.Boolean(default=false)
    created_at = models.DateTimeFields(auto_now_add=True)
    updated_at = models.DateTimeFields(auto_now=True)

    def _str__(self):
        return f'({self.account_id}) - ({self.customer}) - ({self.amount}) - ({self-is_loan}) - ({self.created_at}) - ({self.updated_at})'

class Staffs(models.Model):
    staff_id = models.Field(primary_key=True, default=id, editable=False)
    email = models.CharField(max_length = 30)
    password = models.CharField(max_length = 100)
    

    def __str__(self):
        return f'({self.staff_id}) - ({self.email}) - ({self.password})'

class Ledger(models.Model):
    account_id = models.ForeignKey(Accounts, in_delete=models.CASCADE)
    ledger_id = models.Field(promary_key=True, default=id, editable=False)
    amount = models.DecimalField(max_digit=None, Decimal_places=None)
    description = models.CharField(max_length=100)
    created_at = models.DateTimeFields(auto_now_add=True)

    def __str__(self):
        return f'({self.account_id}) - ({self.ledger_id}) - ({self.amount}) - ({self.description}) - ({self.created_at})'


# Create your models here.
